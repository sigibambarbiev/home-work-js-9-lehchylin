
const tabsLabel = document.querySelector('.tabs-list')
const tabsContent = document.querySelector('.tabs-content');
const tabsContentItems = [...tabsContent.children];

tabsLabel.addEventListener('click', e => {
    const target = e.target;

    if (target !== tabsLabel) {
        [...tabsLabel.children].forEach(item => {
            item.classList.remove('active');
        });

        tabsContentItems.forEach(tab => {
            console.log(tab.dataset.tabs);
            console.log(target.dataset.tabs);
            console.log(tab.dataset.tabs === target.dataset.tabs)
            if (tab.dataset.tabs === target.dataset.tabs) {
                tab.classList.add('active');
            } else {
                tab.classList.remove('active');
            }
        });

        target.classList.toggle('active');
    }

})




